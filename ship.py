import copy
from cell import Cell
from field import Field


class Ship:
    size = 0
    element_placement = []         # array of tuples that contain positioning in relation to 0 point
    body = []

    def __init__(self, field: Field):
        self.body = []             # array of cells taken by current ship
        self.home_field = field
        # hack that avoid update of element_placement on level of whole class
        # when several instances of class are added
        self.elements = copy.copy(self.element_placement)

    def __str__(self):
        return f'{self.__class__.__name__}: {str(self.body)}'

    def set_on_field(self, init_pos_x: int, init_pos_y: int):
        pos_x = 0
        pos_y = 0

        self.elements.insert(0, (init_pos_x, init_pos_y))
        for shift_pos_x, shift_pos_y in self.elements:
            pos_x += shift_pos_x
            pos_y += shift_pos_y
            cell = self.home_field[pos_x, pos_y]
            # If cell exists and ship part can be placed there - adding it to ship body list
            if cell and cell.can_set_ship():
                self.body.append(cell)
        # If all cells exist and there is no adjoining with other ships
        # marking body cells as ship
        if len(self.body) == self.size and self.has_no_adjoining():
            for element in self.body:
                element.set_ship()
            return self.body
        self.body = []          # clearing body so next placement attempt would start from scratch
        del self.elements[0]    # removing zero point for same reasons
        return False

    def count_hp(self):
        return len([cell for cell in self.body if cell.state == Cell.state_ship])

    def get_cells_around(self, ship_cell: Cell) -> list:
        """Getting existing cells around current selected one to avoid collision
        or process ship destruction"""
        cells_around = []
        cells_around_pos = [
            (0, -1),
            (1, -1),
            (1, 0),
            (1, 1),
            (0, 1),
            (-1, 1),
            (-1, 0),
            (-1, -1)
        ]

        for shift_pos_x, shift_pos_y in cells_around_pos:
            pos_x = ship_cell.pos_x + shift_pos_x
            pos_y = ship_cell.pos_y + shift_pos_y
            cell_nearby = self.home_field[pos_x, pos_y]
            if not cell_nearby:
                continue
            cells_around.append(cell_nearby)
        return cells_around

    def has_no_adjoining(self):
        for own_cell in self.body:
            for cell_to_check in self.get_cells_around(own_cell):
                if cell_to_check.is_ship() and cell_to_check not in self.body:
                    return False
        return True

    def disable_cells_around(self, ship_cell: Cell) -> None:
        """After ship blows up - it should not be possible to hit any cells around it
        so we will mark all empty cells around as miss shots to disable further shots there"""
        cells_around = self.get_cells_around(ship_cell)
        for cell in cells_around:
            if cell.is_empty():
                cell.hit()

    def try_to_kill(self):
        """Will be used on each successful ship hit. If ship has no un-hit blocks left
        it's marked as blown up for different visualisation on field"""
        if self.count_hp() > 0:
            return False
        for ship_cell in self.body:
            ship_cell.blow_ship_up()
            self.disable_cells_around(ship_cell)
        return self


class Destroyer(Ship):
    size = 1


class Cruiser(Ship):
    size = 2


class Battleship(Ship):
    size = 3


class Carrier(Ship):
    size = 4


class DestroyerType0(Destroyer):
    pass


class CruiserTypeV(Cruiser):
    element_placement = [(0, 1)]


class CruiserTypeH(Cruiser):
    element_placement = [(1, 0)]


class BattleshipTypeV(Battleship):
    element_placement = [(0, 1), (0, 1)]


class BattleshipTypeH(Battleship):
    element_placement = [(1, 0), (1, 0)]


class BattleshipTypeL(Battleship):
    element_placement = [(0, 1), (1, 0)]


class CarrierTypeH(Carrier):
    element_placement = [(1, 0), (1, 0), (1, 0)]


class CarrierTypeV(Carrier):
    element_placement = [(0, 1), (0, 1), (0, 1)]


class CarrierTypeL(Carrier):
    element_placement = [(0, 1), (0, 1), (1, 0)]


class CarrierTypeZ(Carrier):
    element_placement = [(0, 1), (1, 0), (0, 1)]


class CarrierTypeB(Carrier):
    element_placement = [(0, 1), (1, 0), (0, -1)]


class CarrierTypeT(Carrier):
    element_placement = [(1, 0), (0, 1), (1, -1)]
