from random import choice
from player import Player, AIPlayer


class Game:
    """Initiates players; processes turns and game over"""
    def __init__(self):
        self.player_1 = Player()
        self.player_2 = AIPlayer()
        self.active_player = choice([self.player_1, self.player_2])

    @property
    def on_hold_player(self):
        """Set player that's waiting for the turn to be different from active one"""
        return self.player_2 if self.active_player == self.player_1 else self.player_1

    def play(self):
        """Game turns logic"""
        self.active_player.place_ships()
        self.on_hold_player.place_ships()

        while True:
            if self.active_player.shoot_at_another_player(self.on_hold_player):
                self.on_hold_player.update_ships_status()
                if self.on_hold_player.has_lost():
                    if self.on_hold_player.is_human:
                        print('AI has won!')
                    else:
                        print('Human has won!')
                    break
            else:
                self.active_player = self.on_hold_player
