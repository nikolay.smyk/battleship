class Cell:
    state_empty = 0         # there is nothing, only calm water
    state_ship = 1          # part of the ship is placed here
    state_hit_ship = 2      # this part of ship is burning, but ship is still afloat
    state_miss = 3          # already shot here, appeared to be nothing there
    state_dead_ship = 4     # all ship parts were hit, it's dead now

    visual_states = {
        state_empty: ' ',
        state_ship: '▇',
        state_hit_ship: '☒',
        state_dead_ship: '╳',
        state_miss: '•'
    }

    state_labels = {
        state_empty: 'empty',
        state_ship: 'ship',
        state_hit_ship: 'hit_ship',
        state_dead_ship: 'dead_ship',
        state_miss: 'missed'
    }

    def __init__(self, pos_x, pos_y, state=None):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.state = self.state_empty if state is None else state

    def __repr__(self):
        return f'Cell({self.pos_x}, {self.pos_y}, {self.state})'

    def __str__(self):
        return f'({self.pos_x}, {self.pos_y}): {self.get_label()}'

    def is_empty(self):
        return self.state == self.state_empty

    def is_ship(self):
        return self.state == self.state_ship

    def coordinates(self) -> tuple[int, int]:
        return self.pos_x, self.pos_y

    def hit(self):
        if self.state == self.state_empty:
            self.state = self.state_miss
            return self
        if self.state == self.state_ship:
            self.state = self.state_hit_ship
            return self
        return False

    def blow_ship_up(self):
        if self.state == self.state_hit_ship:
            self.state = self.state_dead_ship

    def get_visual(self):
        return self.visual_states[self.state]

    def get_label(self):
        return self.state_labels[self.state]

    def can_set_ship(self):
        return self.state == self.state_empty

    def set_ship(self):
        if self.state == self.state_empty:
            self.state = self.state_ship
            return self
        return False

    def set_empty(self):
        self.state = self.state_empty
        return self
