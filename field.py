import copy
from cell import Cell


class Field:
    size_x = 10
    size_y = 10

    def __init__(self):
        self.body = [[Cell(y, x, Cell.state_empty) for y in range(self.size_y)] for x in range(self.size_x)]

    def __str__(self):
        return str(self.body)

    def __getitem__(self, item):
        pos_x, pos_y = item
        try:
            cell = self.body[pos_y][pos_x]
        except IndexError:
            return False
        else:
            return cell

    def visualise_body(self, body_to_show):
        body = body_to_show
        h_grid = '━'
        v_grid = '┃'
        x_grid = '╋'
        b_grid = '┻'
        r_grid = '┫'
        rb_grid = '┛'

        header = [v_grid.join(['   '] + [f' {str(y)} ' for y in range(self.size_y)] + [''])]
        lines = [h_grid.join(
            [f' {str(y)} ' + v_grid + v_grid.join([f' {body[y][x].get_visual()} ' for x in range(self.size_x)] + [''])]
        ) for y in range(self.size_y)]
        limiter = '\n' + x_grid.join([h_grid * 3] * (self.size_x + 1)) + r_grid + '\n'
        footer = '\n' + b_grid.join([h_grid * 3] * (self.size_x + 1)) + rb_grid
        print(limiter.join(header + lines) + footer)

    def visualise_as_own(self):
        self.visualise_body(self.body)

    def visualise_as_enemy(self):
        """Hiding ships on enemy field from player"""
        body_to_show = []
        for row in self.body:
            line = []
            for cell in row:
                dummy_cell = copy.copy(cell)     # create dummy cells to avoid interference with field body
                if cell.is_ship():
                    dummy_cell.set_empty()
                line.append(dummy_cell)
            body_to_show.append(line)
        self.visualise_body(body_to_show)

    def get_all_cells_by_state(self, cell_state: int) -> list:
        res = []
        for row in self.body:
            for cell in row:
                if cell.state == cell_state:
                    res.append(cell)
        return res

    def count_ship_cells(self):
        return len(self.get_all_cells_by_state(Cell.state_ship))

    def get_all_empty_cells(self):
        return self.get_all_cells_by_state(Cell.state_empty)
