from random import choices, choice
from field import Field
from ship import Destroyer, Cruiser, Battleship, Carrier


class Player:
    """Joining all the action logic within single entity to use it as single interface
    in the game"""
    # Forming a set of ships to be placed on the field when game start as (ship_type, count)
    ship_count = [
        (Destroyer, 4),
        (Cruiser, 3),
        (Battleship, 2),
        (Carrier, 1)
    ]
    is_human = True

    def __init__(self):
        self.field = Field()
        self.ships = []

    def get_random_ship_set(self):
        """Get randomized set of ship types based on ship type-count rules"""
        player_ships = []
        for ship_type, count in self.ship_count:
            ship_sub_types = ship_type.__subclasses__()
            for ship in choices(ship_sub_types, k=count):
                player_ships.append(ship)
        return player_ships

    def update_ships_status(self):
        """Check all of own ships, if any has 0 hp - update it to dead_ship status"""
        for ship in self.ships:
            ship.try_to_kill()

    def take_coordinates(self):
        """Takes user input and returns position coordinates as tuple (x, y)"""
        while True:
            raw_input = input('Please enter your coordinates as "x y": ')
            try:
                pos_x, pos_y = [abs(int(char)) for char in raw_input.split()]
            except ValueError:
                print('Your input is invalid, try again')
                continue
            else:
                if self.field[pos_x, pos_y]:
                    return pos_x, pos_y
                print('Make sure your coordinates lay within game field')

    def place_ships(self):
        """Create list of random ship types according to ship type count from rules
        and place these on the player field"""
        set_of_random_ship_types = self.get_random_ship_set()
        # reversing ship order to start placement from biggest one
        for ship_type in set_of_random_ship_types[::-1]:
            ship = ship_type(self.field)
            ship_type_name = ship.__class__.__name__
            if self.is_human:
                print(f'Placing {ship_type_name} (size {ship.size})')
            while True:
                pos_x, pos_y = self.take_coordinates()
                if ship.set_on_field(pos_x, pos_y):
                    # once ship is placed on field - add it to player ships
                    self.ships.append(ship)
                    if self.is_human:
                        self.field.visualise_as_own()
                    break
                if self.is_human:
                    print('It\'s impossible to place ship here, select another point')

    def shoot_at_another_player(self, other_player):
        """Take other player as parameter and perform cell.hit() on its' field
        if cell status allows"""
        enemy_field = other_player.field
        if self.is_human:
            print('Select where to shoot')
            enemy_field.visualise_as_enemy()
        while True:
            pos_x, pos_y = self.take_coordinates()
            cell_to_hit = enemy_field[pos_x, pos_y]
            if cell_to_hit.hit():
                other_player.update_ships_status()
                if self.is_human:
                    enemy_field.visualise_as_enemy()
                break
            if self.is_human:
                print('It\'s impossible to shoot here, try another cell')

    def has_lost(self) -> bool:
        """Returns true if player has no un-hit ship cells left"""
        return bool(self.field.count_ship_cells())


class AIPlayer(Player):
    """A copy of player with coordinates selection replaced by randomizer"""
    is_human = False

    def take_coordinates(self) -> tuple[int, int]:
        """Will select random cell from pool of available empty cells
        and return its' coordinates"""
        cell = choice(self.field.get_all_empty_cells())
        return cell.coordinates()
